<?php
	$baseAddress = "./headerImages";

	function get_dirlist( $path, $match = '*', $exclude = array( '.', '..' ) )
	{
	  $result = array();

	  if( ( $handle = opendir( $path ) ) )
	  {
		while( false !== ( $fname = readdir( $handle ) ) )
		{
	      $lowerfname = strtolower($fname);
		  $skip = false;

		  if( !empty( $exclude ) )
		  {
			if( !is_array( $exclude ) )
			  $skip = fnmatch( $exclude, $lowerfname );
			else
			{
			  foreach( $exclude as $ex )
			  {
				if( fnmatch( $ex, $lowerfname ) )
				  $skip = true;
			  }
			}
		  }

		  if( !$skip && ( empty( $match ) || fnmatch( $match, $lowerfname ) ) )
			$result[] = $fname;
		}

		closedir( $handle );
	  }

	  return $result;
	}

	$resized = $baseAddress . "/thumb/";
	$original = $baseAddress . "/full/";

	$list = get_dirlist($resized, '*.jpg');
	//Randomizes the list array, as the rand_index will choose 
	//random, but in order.  This makes it seem more random.
	shuffle($list);  

	//Chooses three random list, and prints them
	$rand_index = array_rand($list,3);
	$format0 = "<a href='";
	$format1 = "'><img src='";
	$format2 = "' width='240' height='160' alt='' title=''></a>\n";
	
	print $format0 . $original.$list[$rand_index[0]] . $format1 . $resized.$list[$rand_index[0]] . $format2;
	print $format0 . $original.$list[$rand_index[1]] . $format1 . $resized.$list[$rand_index[1]] . $format2;
	print $format0 . $original.$list[$rand_index[2]] . $format1 . $resized.$list[$rand_index[2]] . $format2;
?>

