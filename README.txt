Started On: September 1, 2010

This script takes two directories, one with small images, and one with full size
counterparts and randomly selects images from it. This is handy if you have a 
lot of images for a location, but only wish to display a some of them at a time.